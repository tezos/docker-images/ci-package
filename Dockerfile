FROM docker:20-git

# hadolint ignore=DL3018,DL3028
RUN apk add --no-cache ruby aws-cli curl bash alpine-sdk xz; \
    apk add --no-cache createrepo_c --repository=http://dl-cdn.alpinelinux.org/alpine/edge/testing; \
    rm -rf /var/cache/apk/*; \
    curl -sLO https://github.com/deb-s3/deb-s3/releases/download/0.11.3/deb-s3-0.11.3.gem; \
    gem install deb-s3-0.11.3.gem; \
    gem install nokogiri
