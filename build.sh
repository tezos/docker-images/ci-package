#!/bin/bash
set -euo pipefail

# Bash fork from Kaniko GitLab template
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Kaniko.gitlab-ci.yml

if [[ "${CI_COMMIT_REF_NAME}" == "${CI_DEFAULT_BRANCH}" ]]
then
  DOCKER_IMAGE_TAG='latest'
elif [[ -n "${CI_COMMIT_TAG:-}" ]]
then
  NOSLASH=$(echo "${CI_COMMIT_TAG}" | tr -s / - )
  SANITIZED="${NOSLASH//[^a-zA-Z0-9\-\.]/}"
  DOCKER_IMAGE_TAG="${SANITIZED}"
else \
  NOSLASH=$(echo "${CI_COMMIT_REF_NAME}" | tr -s / - )
  SANITIZED="${NOSLASH//[^a-zA-Z0-9\-]/}"
  DOCKER_IMAGE_TAG="branch-${SANITIZED}"
fi

## Docker JSON configuration for Amazon ECR using credential helper

# Create directory for user Docker JSON configuration
mkdir -pv ~/.docker

# Template out Docker JSON configuration
envsubst -no-unset -no-empty -i docker-config.json -o ~/.docker/config.json

# Validate Docker JSON configuration
jq empty ~/.docker/config.json

### Log in to GitLab container registry
docker login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" registry.gitlab.com

## Build multi-arch Docker image

gitlab="${CI_REGISTRY_IMAGE}:${DOCKER_IMAGE_TAG}"
aws="${AWS_ECR_IMAGE}:${DOCKER_IMAGE_TAG}"

# Cross-platform emulator collection
# We cannot make this installation permanent inside the GitLab executor AWS AMI yet
# https://github.com/tonistiigi/binfmt/issues/75
docker run --privileged --rm "tonistiigi/binfmt:qemu-v${BINFMT_VERSION}" --install all

docker buildx create --use --name tezos || echo "Warning: Docker builder instance already exists"

# Build and push to GitLab container registry and AWS ECR
docker buildx build --push \
                    --platform linux/amd64 \
                    --tag "${gitlab}" \
                    --tag "${aws}" \
                    -f Dockerfile .

docker buildx build --push \
                    --platform linux/amd64 \
                    --tag "${gitlab}-fedora" \
                    --tag "${aws}-fedora" \
                    -f Dockerfile.fedora .

docker buildx build --push \
                    --platform linux/amd64 \
                    --tag "${gitlab}-ubuntu" \
                    --tag "${aws}-ubuntu" \
                    -f Dockerfile.ubuntu .
